use std::process::Stdio;
use anyhow::anyhow;
use futures_util::future::join_all;
use tokio_stream::StreamExt;
use tokio_util::codec::{FramedRead, LinesCodec};
#[tokio::main]
async fn main(){

    let args: Vec<String> = std::env::args().collect::<Vec<String>>().iter().skip(1).map(|x| x.to_string()).collect();
    let mut threads = vec![];
    for arg in args.iter(){
        let range = 0..24;
        for hour in range {
            threads.push(call_process(arg.clone(), hour.to_string()));
        }
    }

    let res = join_all(threads).await;
    if let Some(arg) = res.iter().find(|x| x.is_ok()) {
        println!("success: Key is {}", arg.as_ref().unwrap());
    } else {
        println!("none of the keys {:?} were correct ", args);
    }
}

async fn call_process(arg: String, hour: String) -> anyhow::Result<String> {
    let mut process = tokio::process::Command::new("/home/chris/.nvm/versions/node/v18.17.1/bin/node")
        .arg("decryp-hour.js")
        .arg(hour)
        .arg(arg.clone())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .stdin(Stdio::piped())
        .spawn()
        .unwrap();


    let stdout = FramedRead::new(process.stdout.take().unwrap(), LinesCodec::new())
        .map(|data| data.expect("fail on out!"));

    let stderr = FramedRead::new(process.stderr.take().unwrap(), LinesCodec::new())
        .map(|data| data.expect("fail on err!"));


    let mut stream = stdout.chain(stderr);

    while let Some(msg) = stream.next().await {
        println!("{}: {:?}", arg, msg);
        if msg.contains("status") && msg.contains("true") {
            return Ok(arg);
        }
    }
    let exit_status = process.wait().await;
    println!("----------------------------------------- {}: {:?}", arg, exit_status);
    Err(anyhow!(" {}: fail", arg))
}
